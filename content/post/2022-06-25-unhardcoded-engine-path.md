---
title: Unhardcoded engine path
date: 2022-06-25
comments: false
authors:
 - illwieckz
image: /img/screenshots/20220628-184619-000.unhardcoded-path-gamepack.png
---

![Unhardcoded engine path](/img/screenshots/20220628-184619-000.unhardcoded-path-gamepack.png)

It's now possible to use some keywords in NetRadiant gamepacks to set engine folder that may differ according to the user configuration.

For example it is possible to set paths using `%ProgramFiles%`, `${HOME}`, `${XDG_DATA_HOME}` and other things like that in a way the string is rewrote according to the user environment.

Maybe one day this feature will be usable for game user directory too, we'll see!

Note: this feature may not be implemented in other Radiant so a gamepack making use of this feature is specific to compatible editors like upstream NetRadiant.
