---
title: CRN and WebP image support
date: 2018-01-06
comments: false
authors:
 - illwieckz
image: /img/screenshots/20150715-063954-000.nautilus-crn-thumbnails.png
---

![CRN Textures](/img/screenshots/20150715-063954-000.nautilus-crn-thumbnails.png)

The CRN format support was added to both NetRadiant and Q3map2. It is implemented by using the [Crunch submodule from Dæmon Engine](https://github.com/DaemonEngine/crunch) which follows [Unity's implementation](https://github.com/Unity-Technologies/crunch/tree/unity). Also, the WebP image support is now merged. It was implemented long time ago and distributed with [Ingar's builds](http://ingar.intranifty.net/gtkradiant/) for years, but was not merged until now. Also, NetRadiant was made more resilient by not erroring on missing image support.

The CRN format is an image compression format optimized for GPU loading like DDS or KTX, hence it is likely to be used by games. The WebP is an image format that was first designed with Web in mind, but being adopted by more and more software as it supports both lossless and lossy compression while having better compression ratio than PNG on lossless mode, and with lossy mode it produces less artifacts while saving file size compared to a JPG. Also, unlike JPG, WebP supports alpha channel on lossy mode. Making NetRadiant and Q3map2 able to support those formats makes the map editor able to display game data using those formats and makes the map compiler able to build against them.

See related threads about Crunch implementation: [crunch as submodyle](https://gitlab.com/xonotic/netradiant/merge_requests/104), [crunch image plugin](https://gitlab.com/xonotic/netradiant/merge_requests/99), [webp inclusion](https://gitlab.com/xonotic/netradiant/merge_requests/100), [missing image plugin resilience](https://gitlab.com/xonotic/netradiant/merge_requests/105).
