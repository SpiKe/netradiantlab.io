---
title: User path change
date: 2022-06-19
comments: false
authors:
 - illwieckz
image: /img/screenshots/20220628-190257-000.netradiant-appdata.png
---

![NetRadiant AppData path](/img/screenshots/20220628-190257-000.netradiant-appdata.png)

On Windows the user directory is now saved as `%AppData%/NetRadiant` (for example: `C:\Users\<username>\AppData\Roaming\NetRadiant`) which makes more sense and avoids conflicting with NetRadiant Custom directory to prevent each other to rewrite the same configuration files (NetRadiant Custom is another NetRadiant fork).

The old directory was named `NetRadiantSettings` in the same parent folder. If you were not using other NetRadiant fork or you know your configuration is fine, you can recover your configuration by renaming the folder `NetRadiant`.

On macOS, the user directory is now `~/Library/Application Support/NetRadiant`. It was previously using a Linux-like XDG folder because specific macOS code was missing.

As a remind, on Linux and FreeBSD the user directory is now an XDG folder (`~/.config/netradiant`) since years. It was previously stored in `~/.netradiant/1.5.0`.
