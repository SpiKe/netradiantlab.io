---
title: Autorestart on game or layout change
date: 2019-07-22
comments: false
authors:
 - illwieckz
image: /img/screenshots/20200120-033140-000.netradiant-autorestart.png
---

![NetRadiant auto restart](/img/screenshots/20200120-033140-000.netradiant-autorestart.png)

NetRadiant is now able to restart itself when game is changed or a new layout is selected, asking the user for confirmation and reloading the current map.

This makes easy to change the window layout since it will just be applied and reloaded using the current map, and it makes easier to switch the game after loading a map, which is very convenient when the map is loaded thanks to file association but NetRadiant is started with the wrong game.

See the [merge thread](https://gitlab.com/xonotic/netradiant/merge_requests/141) and [some fixes](https://gitlab.com/xonotic/netradiant/merge_requests/155) (thanks to Garux!) for more details about the implementation.
