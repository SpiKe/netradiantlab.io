---
title: Clickable Linux binary
date: 2021-03-07
comments: false
authors:
 - illwieckz
image: /img/screenshots/20220628-194213-000.netradiant-clickable-linux-binary.png
---

![Clickable Linux binary](/img/screenshots/20220628-194213-000.netradiant-clickable-linux-binary.png)

Because [of a bug](https://gitlab.freedesktop.org/xdg/shared-mime-info/-/issues/11) in a common Linux desktop component, the NetRadiant binary was not clickable on some Linux systems: user had to run NetRadiant from command line, not from the file manager (the binary was wrongly detected as a shared library!).

This is worked around by producing the binary with another format that is properly recognized as binary by file managers and then clickable.
