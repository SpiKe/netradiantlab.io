---
title: Modifiable Field of View
date: 2021-06-27
comments: false
authors:
 - illwieckz
image: /img/screenshots/20220628-185133-000.modifiable-camera-fov.png
---

![Modifiable NetRadiant camera field of view](/img/screenshots/20220628-185133-000.modifiable-camera-fov.png)

Thanks to ballerburg9005 it's now possible to adjust field of view (FOV) in camera preferences!
