---
title: 2020 Easter builds
date: 2020-04-19
comments: false
authors:
 - illwieckz
image: /img/screenshots/20200419-173703-000.netradiant-smokinguns-sweetwater.1600x900.jpg
---

![Sweetwater Smokin' Guns map made with NetRadiant](/img/screenshots/20200419-173703-000.netradiant-smokinguns-sweetwater.1600x900.jpg)

2020 Easter builds are available, see the [**download page**](/page/download)!

Thanks to Barto and Breli, issues in both Smokin' Guns gamepack and `.tex` support in map compiler were fixed! You may checkout [the latest map by Breli, _“Sweetwater”_](https://www.smokin-guns.org/breli-map-publishing), made with NetRadiant! This map is an hommage to one of the greatest movies of all time!

On the same time, divVerent fixed an issue reported by Garux about [generated grid light not working](https://gitlab.com/xonotic/netradiant/-/issues/137) on Quake III Arena legacy renderer.

Along with the testing by those Western guys, I would like to thank Spike29 and Ronan from the Xonotic mapping channel for the various reports about the builds themselves.

![Minimap for Unvanquished chasm map](/img/screenshots/20200419-183003-000.netradiant-unvanquished-chasm-minimap.png)

Minimap generation for Unvanquished is now complete. Before, you had to write by hand a configuration file based on some numbers q3map2 was printing on console, now q3map2 writes down that file for you! DaemonMap integration with build menu was implemented, so now you can generate navmeshes with a single click on the right menu entry!

On the UI side, you'll maybe notice that most of the pop-ups are now appearing at the center of the main window, not the center of the screen. Owners of large screens will appreciate this!

Those builds are for Linux and Windows, and include a selection of various game packs like Xonotic, Unvanquished, Warsow, Wolf: ET, Kingpin, Quake 3, Quetoo, Smokin' Guns, Tremulous, OpenArena, Osirion, and others.

![NetRadiant with World of Padman gamepack](/img/screenshots/20200406-094032-000.netradiant-world-of-padman-gamepack.png)

Note that some gamepacks may not be tested a lot. The Quake 3 gamepack shipped in those builds was updated manually using entity definition file from Garux's fork, which is known to be more up-to-date than the one fetched by the build script. Some gamepacks were manually added like those for Quake Live and World of Padman but they are shipped “_as is_”, they may ask for some improvements, but that's better than nothing…

Don't wait more! **[→ Go to the downloads](/page/download)**!
