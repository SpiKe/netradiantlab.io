---
title: Fixed PNG cheap transparency
date: 2019-06-02
comments: false
authors:
 - illwieckz
image: /img/screenshots/20200120-183921-000.netradiant-pngalpha.png
---

![Proper PNG transparency](/img/screenshots/20200120-183921-000.netradiant-pngalpha.png)

A bug was found in PNG image plugin of NetRadiant. This was affecting alpha channel stored in tRNS PNG chunk, also know as PNG cheap transparency. The PNG format has many storage options and its space-saving efficiency mainly comes from the ability optimizers have to select the storage format that saves the most file space for a given texture.

Unfortunately, those various formats are rarely used because a very few if not almost none editing tools having PNG export option attempt to optimize them. Because of those variants being rare, they are less tested.

The fix was implemented in NetRadiant, Q3map2 was verified to not reproduce the bug.

A [test map](https://github.com/UnvanquishedAssets/UnvanquishedTestAssets/tree/master/src/map-test-pngalpha_src.dpkdir) using textures in various PNG formats was made to be used as a testbed for level editors, map compilers, and game engines.

Basically, if the map random this way (colored background may use a random selected color on ever map load), you know the NetRadiant build you use ships the bug:

![NetRadiant with cheap transparency bug](/img/screenshots/201912-06-07-37-32.netradiant-png-cheap-transparency.png)
