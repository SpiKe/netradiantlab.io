---
title: macOS debut
date: 2021-02-20
comments: false
authors:
 - illwieckz
image: /img/screenshots/20211109-163446-000.netradiant-macos.png
---

![NetRadiant on macOS](/img/screenshots/20211109-163446-000.netradiant-macos.png)

It's now possible to build for macOS with bundling of libraries. It relies on a [custom branch of GtkGLExt](https://gitlab.gnome.org/illwieckz/gtkglext/-/tree/macos) which is a bit buggy (euphemism) but fortunately most of the bugs can be worked around.

That build does not require X11 (XQuartz), it's a native GTK build with non-X11 OpenGL.

A Mojave-like GTK theme can be bundled with both light and dark variant, the editor selects the theme variant at startup according to the current desktop preference.

There may be problems with multi-screen configurations.
