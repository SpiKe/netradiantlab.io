---
title: Download NetRadiant
subtitle: Dowload and start mapping!
comments: false
authors:
 - xonotic
 - illwieckz
image: /img/screenshots/20200120-020950-000.netradiant-windows-build.png
---

![NetRadiant Windows build](/img/screenshots/20200120-020950-000.netradiant-windows-build.png)

## NetRadiant builds

Those are built from time to time by [illwieckz](/author/illwieckz), in an attempt to provide up-to-date NetRadiant builds.

### Download NetRadiant 1.5.0-20220628

Linux users are expected to have an already working GTK2 and OpenGL environment[^1]. Windows users can enable optional Mesa3D rendering[^2].

- **Linux 64-bit**: [EU download][linux-amd64/eu], [US download][linux-amd64/us].
- **Windows 64-bit**: [EU download][windows-amd64/eu], [US download][windows-amd64/us].
- **Windows 32-bit**: [EU download][windows-i686/eu], [US download][windows-i686/us].
- **macOS 64-bit**: [EU download][macos-amd64/eu], [US download][macos-amd64/us].
- **FreeBSD 64-bit**: [EU download][freebsd-amd64/eu], [US download][freebsd-amd64/us].
- **Checksums:** [EU download][sha512sums/eu], [US download][sha512sums/us].

[linux-amd64/eu]:https://dl.illwieckz.net/share/netradiant/release/netradiant_1.5.0-20220628-linux-amd64.tar.xz
[windows-amd64/eu]:https://dl.illwieckz.net/share/netradiant/release/netradiant_1.5.0-20220628-windows-amd64.zip
[windows-i686/eu]:https://dl.illwieckz.net/share/netradiant/release/netradiant_1.5.0-20220628-windows-i686.zip
[macos-amd64/eu]:https://dl.illwieckz.net/share/netradiant/release/netradiant_1.5.0-20220628-macos-amd64.tar.xz
[freebsd-amd64/eu]:https://dl.illwieckz.net/share/netradiant/release/netradiant_1.5.0-20220628-freebsd-amd64.tar.xz

[linux-amd64/us]:https://dl.unvanquished.net/share/netradiant/release/netradiant_1.5.0-20220628-linux-amd64.tar.xz
[windows-amd64/us]:https://dl.unvanquished.net/share/netradiant/release/netradiant_1.5.0-20220628-windows-amd64.zip
[windows-i686/us]:https://dl.unvanquished.net/share/netradiant/release/netradiant_1.5.0-20220628-windows-i686.zip
[macos-amd64/us]:https://dl.unvanquished.net/share/netradiant/release/netradiant_1.5.0-20220628-macos-amd64.tar.xz
[freebsd-amd64/us]:https://dl.unvanquished.net/share/netradiant/release/netradiant_1.5.0-20220628-freebsd-amd64.tar.xz

[sha512sums/eu]:https://dl.illwieckz.net/share/netradiant/release/netradiant_1.5.0-20220628.sha512sum
[sha512sums/us]:https://dl.illwieckz.net/share/netradiant/release/netradiant_1.5.0-20220628.sha512sum

Those builds fully support released Xonotic files so there is no need to download an extra mapping support package anymore. The `daemonmap` tool for Unvanquished navmesh generation is included. Smokin' Guns q3map2 patches are included.

First class compatibility is provided for games like [Xonotic](https://xonotic.org) and [Unvanquished](https://unvanquished.net), about 30 other games are supported (support may vary).

Those builds are now to be prefered over Ingar's builds[^3].

See the [related forum thread](https://forums.xonotic.org/showthread.php?tid=8108) for more information about those builds.

[^1]: To get a proper GTK2 with OpenGL environment on Linux, install package `libgtkglext1` on Ubuntu, `gtkglext-libs` on Fedora, `gtkglext` on Arch and Solus, `libgtkglext-x11-1_0-0` on openSUSE, or `x11-libs/gtkglext` on Gentoo. If you run another distribution, look for the _GtkGLExt_ package, this must also bring required dependencies. Other libraries are bundled.

[^2]: People with old Intel chip on Windows 10 may face OpenGL issue and unusable white 3D/2D views, those people can workaround the issue by moving everything from `mesa3d/` subdirectory right to `netradiant.exe` to enable software rendering fallback.

[^3]: Ingar [provided](http://ingar.intranifty.net/gtkradiant/) NetRadiant builds and gamepacks for a long time. Those builds are now severly outdated and newer builds are now to be preferred instead.
